FROM python:3.8-alpine

WORKDIR /app

RUN apk --no-cache add gcc musl-dev python3-dev libffi-dev openssl-dev
COPY requirements.txt /
RUN pip install --no-cache-dir -r /requirements.txt && rm /requirements.txt
RUN apk del gcc musl-dev python3-dev libffi-dev openssl-dev

COPY . /app

EXPOSE 80

CMD [ "python", "/app/run", "--flask-host=0.0.0.0", "--flask-port=80"]