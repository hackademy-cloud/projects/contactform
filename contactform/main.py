import flask
from flask import Flask, redirect
from flask_restful import reqparse
import smtplib
from email.message import EmailMessage

import telegram

from contactform.config import args_config, env_config

app = Flask(__name__)
bot = telegram.Bot(token=env_config.bottoken)


class Contact:
    def __init__(self, firstname: str, lastname: str,
                 email: str, company: str,
                 jobtitle: str, phone: str):
        self.firstname = firstname
        self.lastname = lastname
        self.email = email
        self.company = company
        self.jobtitle = jobtitle
        self.phone = phone

    def to_dict(self):
        return self.__dict__


@app.route('/contact', methods=["POST"])
def contact():
    parser = reqparse.RequestParser()
    parser.add_argument('firstname', required=True, type=str)
    parser.add_argument('lastname', required=True, type=str)
    parser.add_argument('email', required=True, type=str)
    parser.add_argument('company', required=True, type=str)
    parser.add_argument('jobtitle', required=True, type=str)
    parser.add_argument('phone', required=True, type=str)
    args = parser.parse_args(strict=True)
    # CREATE MSG
    msg = "NEW CONTACT:\n"
    msg += "\nName: {firstname} {lastname}"
    msg += "\nEmail: {email}"
    msg += "\nCompany: {company}"
    msg += "\nJob Title: {jobtitle}"
    msg += "\nPhone: {phone}"
    contact = Contact(**args)
    out = msg.format(**contact.to_dict())
    # SEND MAIL
    msg = EmailMessage()
    msg.set_content(out)
    msg['Subject'] = 'New Contact'
    msg['From'] = env_config.msg_from
    msg['To'] = env_config.msg_to
    s = smtplib.SMTP_SSL(env_config.smtpserver, port=env_config.smtpport)
    s.login(env_config.smtpusername, env_config.smtppassword)
    s.send_message(msg)
    s.quit()
    # SEND TG MSG
    bot.send_message(chat_id=env_config.telegramchatid, text=out)
    # REDIRECT
    return redirect(env_config.redirect_url, code=302)


def main():
    app.run(host=args_config.flask_host, port=args_config.flask_port)


if __name__ == '__main__':
    main()