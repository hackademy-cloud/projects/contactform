import argparse
import os
from dataclasses import dataclass

from dotenv import load_dotenv


@dataclass
class ArgsConfig:
    flask_host: str = "localhost"
    flask_port: str = "8082"


def _parse_args():
    parser = argparse.ArgumentParser(
        description="Adds an api to manage courses.")
    # configs
    parser.add_argument("--flask-host",
                        nargs="?",
                        help="host where the api is running",
                        action="store",
                        default=ArgsConfig.flask_host)
    parser.add_argument("--flask-port",
                        nargs="?",
                        help="port where the api is running",
                        action="store",
                        default=ArgsConfig.flask_port)
    return parser.parse_args()


@dataclass
class EnvConfig:
    bottoken: str
    telegramchatid: str
    smtpserver: str
    smtpport: str
    smtpusername: str
    smtppassword: str
    msg_to: str
    msg_from: str
    redirect_url: str


_args = _parse_args()
_args_config = ArgsConfig(
    flask_host=_args.flask_host,
    flask_port=_args.flask_port
)

_basedir = os.path.abspath(os.path.dirname(__file__))
_file = os.path.join(_basedir, '../.env')
if os.path.isfile(_file):
    load_dotenv(_file)

_env_config = EnvConfig(
    bottoken=os.environ["BOTTOKEN"],
    telegramchatid=os.environ["TELEGRAMCHATID"],
    smtpserver=os.environ["SMTPSERVER"],
    smtpport=os.environ["SMTPPORT"],
    smtpusername=os.environ["SMTPUSERNAME"],
    smtppassword=os.environ["SMTPPASSWORD"],
    msg_from=os.environ["MSGFROM"],
    msg_to=os.environ["MSGTO"],
    redirect_url=os.environ["REDIRURL"]
)

env_config = _env_config
args_config = _args_config